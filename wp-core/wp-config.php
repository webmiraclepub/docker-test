<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress_2');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'password');

/** Имя сервера MySQL */
define('DB_HOST', 'mysql');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         '51^[-z%.DM@G!QV;L;)*8G]Kp$cgn:<)P8=}`#0oMTQDeS}]nng`M&plTklk#GU/');
define('SECURE_AUTH_KEY',  'zl3onvtU`GpoY)I<2<Crss/^:6%p.}jJ=4v9^Jyw1SvSVv:]wer$AIr<SLCfF!sS');
define('LOGGED_IN_KEY',    '174jGi(X1do9_z[;O7}_s=}gF/daJw`F}V{;dY;$&kZKn7d)&AX+8$;[4CL:Z$On');
define('NONCE_KEY',        'Q8*p!do@Kq0daG<qLrr*Pk-DJJQ@o_QT,WZ+TY_JU;r#VtxYh<7Tmfz=>b,wu>!Q');
define('AUTH_SALT',        '|Aq)2pE@(bcMe4}K=OAyKkFw.`>4{iJ3g@y;[L]:Kp}qwU+$:xNW[HL;l/V[d+,f');
define('SECURE_AUTH_SALT', '77zQHJIByFC1P>#2LJOkLs^.jux,6?p0_Jmj54oKaq{WpsXay|c)U|k80L*$Qte^');
define('LOGGED_IN_SALT',   '!p}=/oUv1U:10DA55eA:Kq`/hx-ptO=43tz|].?;ATs?#RvWjih?+^;U]Z5P-<%:');
define('NONCE_SALT',       'F)&=EOhE$c?P*J.Jg6NGl.`q<V{Kn.D/&k=(TD5P%HSY-RwjQDKe*Z<GV=2(f3Z5');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
